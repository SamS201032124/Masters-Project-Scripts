import numpy as np

def periodic_wrap(box,x,y,z):

    index = np.where(x>(box/2))
    if len(x[index])>0:
		x[index] -= box

    index = np.where(y>(box/2))
    if len(y[index])>0:
		y[index] -= box

    index = np.where(z>(box/2))
    if len(z[index])>0:
		z[index] -= box

    index = np.where(x<(-box/2))
    if len(x[index])>0:
		x[index] += box
    
    index = np.where(y<(-box/2))
    if len(y[index])>0:
		y[index] += box

    index = np.where(z<(-box/2))
    if len(z[index])>0:
		z[index] += box

    return x,y,z
