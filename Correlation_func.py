from __future__ import division
import os, os.path
import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import glob
import time
from periodic_wrap import *
start_time = time.time()

#importng the data from the subfind files
path1 = '/hpcdata4/sam/run_0/data/groups_033/eagle_subfind_tab_033.?.hdf5'
path2 = '/hpcdata4/sam/run_0/data/groups_033/eagle_subfind_tab_033.??.hdf5'
path3 = '/hpcdata4/sam/run_0/data/groups_033/eagle_subfind_tab_033.???.hdf5'
a = glob.glob(path1)
m = glob.glob(path2)
n = glob.glob(path3)
o = a+m+n

print('There are ', len(o),' files in the directory')
Coords_x = np.array([])
Coords_y = np.array([])
Coords_z = np.array([])

for i in o:
	filepath = i
	file = h5.File(filepath)
        b = file.get('FOF/GroupCentreOfPotential')
       
        Coords_x = np.append(Coords_x,b[:,0])
        Coords_y = np.append(Coords_y,b[:,1])
        Coords_z = np.append(Coords_z,b[:,2])
        
print 'There are: ',len(Coords_x),' Haloes'

#setting up the coordinates to calculate the DD data set
N_gal = len(Coords_x)
N=len(Coords_x)
Coords_x = Coords_x[:N]
Coords_y = Coords_y[:N]
Coords_z = Coords_z[:N]
radius = np.array([])

#setting up the bins and counts to store the clustering data at certain
#radial bins pre defined so python doesnt change them. 
bins = np.arange(0.1,2.9,0.1)
counts = np.zeros((len(bins)-1))

for i in np.arange(0,N):
    counts_temp = np.array([])
    
    dx = Coords_x-Coords_x[i]
    dy = Coords_y-Coords_y[i]
    dz = Coords_z-Coords_z[i]
    
    dx,dy,dz = periodic_wrap(400,dx,dy,dz)
    
    radius  = ((dx)**2 + (dy)**2 + (dz)**2)**(1/2)
    radius = np.log10((radius[radius>0]))
    
    counts_temp, bin_edges = np.histogram(radius,bins = np.arange(0.1,2.9,0.1))
    counts += counts_temp

#print counts

print "My program took", time.time() - start_time, "to run"
    
#Working out how many galaxies would be in a random distribution if the galaxies were distributed in a homogenous way throughout the simulation
#note this was not explicitly used in the project but was written into the script incase used in future instances.

bins = np.arange(0.1,2.9,0.1)
ave_density = N_gal/(400**3)

vol_array = np.array([])
for i in bins:
	Previous_size = i-0.1
	Vol_of_bins = 4/3 * (np.pi) * ((10**i)**3 - (10**Previous_size)**3)
	vol_array = np.append(vol_array,Vol_of_bins)
print vol_array
Random_gals = ave_density * vol_array[1:13] * N_gal

##Calculating the random distribution of galaxies via random number method
Rand_x = 400*np.random.random((N))
Rand_y = 400*np.random.random((N))
Rand_z = 400*np.random.random((N))
radius = np.array([])
bins = np.arange(0.1,2.9,0.1)

counts_rand = np.zeros((len(bins)-1))

for i in np.arange(0,N):
    counts_temp = np.array([])
    dx = Rand_x - Rand_x[i]
    dy = Rand_y - Rand_y[i]
    dz = Rand_z - Rand_z[i]
    
    dx,dy,dz = periodic_wrap(400,dx,dy,dz)
    
    radius  = ((dx)**2 + (dy)**2 + (dz)**2)**(1/2)
    radius = np.log10((radius[radius>0]))
    counts_temp, bin_edges = np.histogram(radius,bins = np.arange(0.1,2.9,0.1))
    counts_rand += counts_temp
print 'counts_rand',counts_rand[1:13]
print Random_gals
xi = (counts/counts_rand) - 1
print(xi[1:13])
plt.figure(2)
plt.plot(bin_edges[1:13],np.log10(xi[1:13]))
plt.xlabel(r'log(r) [Mpc h$^{-1}$]')
plt.ylabel(r'log($\xi$)')
plt.savefig('2 point correlation function run=0 for L400 simulation')

np.savetxt('xi_data_run_0_L400',xi)