from __future__ import division
import h5py as h5
import numpy as np
import glob

def hmf_counts_func(path,SN,masslim):
        print 'The path is: ',path
        print 'Snapshot number = ',SN
        print 'Mass limit = 10^'+str(masslim)+'Solar Masses'
        #importng the data from the subfind files
        path1 = path+'groups_'+str(SN)+'/eagle_subfind_tab_'+str(SN)+'.?.hdf5'
        path2 = path+'groups_'+str(SN)+'/eagle_subfind_tab_'+str(SN)+'.??.hdf5'
	path3 = path+'groups_'+str(SN)+'/eagle_subfind_tab_'+str(SN)+'.???.hdf5'

        #defining a list with all of the file names stored in to be read in
        a = glob.glob(path1)
        m = glob.glob(path2)
	o = glob.glob(path3)
        n = a+m+o
        print 'There are ', len(n),' files to be read in'

        Coords_x = np.array([])
        Coords_y = np.array([])
        Coords_z = np.array([])
        M_200crit = np.array([])

        for i in n:
	        file = h5.File(i)
                Coords = file.get('FOF/GroupCentreOfPotential')
                Mass = file.get('FOF/Group_M_Crit200')
       
                Coords_x = np.append(Coords_x,Coords[:,0])
                Coords_y = np.append(Coords_y,Coords[:,1])
                Coords_z = np.append(Coords_z,Coords[:,2])
                M_200crit = np.append(M_200crit, Mass)

        print 'There are ', len(Coords_x),' positions in snap: '+str(SN)
        print 'There are ', len(M_200crit),' masses in snap: '+str(SN)

        if len(Coords_x) == len(M_200crit):
                print 'these arrays match up'
        else:
                print 'ERROR: these arrays are not the same size check this'
                exit()

        #Converting the masses to solar logarithmic units first 
        M_200crit = M_200crit*(1E10)
        M_200crit = np.log10(M_200crit)

        #finding the indices of all the haloes between the desired mass range
        index = np.where(M_200crit>=masslim)

        #updating the array to only include these values
        M_200crit = M_200crit[index]

        print 'There are ',len(M_200crit), ' Haloes with masses above 10^'+str(masslim)+' Solar Masses in snap: '+str(SN)
        return M_200crit