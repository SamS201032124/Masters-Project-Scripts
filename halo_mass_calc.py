from __future__ import division
import os, os.path
import h5py as h5
import numpy as np
import matplotlib.pyplot as plt
import glob

path1 = '/hpcdata4/sam/run_0/data/groups_033/eagle_subfind_tab_033.?.hdf5'
path2 = '/hpcdata4/sam/run_0/data/groups_033/eagle_subfind_tab_033.??.hdf5'
path3 = '/hpcdata4/sam/run_0/data/groups_033/eagle_subfind_tab_033.???.hdf5'
a = glob.glob(path1)
m = glob.glob(path2)
n = glob.glob(path3)
o = a+m+n

print('There are ', len(o),' files in the directory')
M_200crit = np.array([])
z = np.array([])
for i in o:
	filepath = i
	file = h5.File(filepath)
        a_group_key = list(file.keys())[0]
        b = file.get('FOF/Group_M_Crit200')
	M_200crit = np.append(M_200crit,b)


M_200crit = M_200crit[M_200crit>0]


#converting to solar units per h:
#Note if want in units of just solar masses, divide by 0.6711
M_200crit = M_200crit*(1E10)
log10M200 = np.log10(M_200crit) #taking natural log of all of the array

binwidth=0.1
plt.figure(1)
x = log10M200[log10M200>13.0]

plt.hist(x,bins=np.arange(min(x),max(x)+binwidth, binwidth) )
counts, bin_edges = np.histogram(x,bins=np.arange(min(x),max(x)+binwidth, binwidth) )

plt.figure(2)
phi_0 = counts/(binwidth*(400**3))

plt.plot(bin_edges[:len(phi_0)],np.log10(phi_0))
plt.xlabel(r'M$_{200crit}  [M_{ \odot} h^{-1}]$')
plt.ylabel(r'log($\phi$) [h$^3 Mpc^{-3}$]')
np.savetxt('phi_0_z_0_L400',phi_0)
plt.savefig('Halo mass function for 0 running L400')